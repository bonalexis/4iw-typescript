# Apprentissage TypeScript

## Commandes

Installation des dépendances :

```sh
npm i
```

Lancement du serveur de développement :

```sh
npm run dev
```

Compilation des source :

```sh
npm run build
```

Lancement du serveur de production :

```sh
npm start
```
