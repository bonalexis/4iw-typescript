type WeatherAttributes = {
  city: string;
  temperatureCelsius?: number;
  weatherCode?: number;
};

type TemperatureUnit = "CELSIUS" | "FAHRENHEIT";

type CoordinatesForCity = {
  city: string;
  latitude: number;
  longitude: number;
};

type WeatherData = {
  latitude: number;
  longitude: number;
  generationtime_ms: number;
  utc_offset_seconds: number;
  timezone: string;
  timezone_abbreviation: string;
  elevation: number;
  current_units: {
    time: string;
    interval: string;
    temperature_2m: string;
    weather_code: string;
  };
  current: {
    time: string;
    interval: number;
    temperature_2m: number;
    weather_code: number;
  };
};
