import { CODES_METEO } from "../app/WeatherCodes";


const COORDINATES_FOR_CITIES: CoordinatesForCity[] = [
  { city: "Lille", latitude: 50.6365654, longitude: 3.0635282 },
  { city: "Paris", latitude: 48.8534951, longitude: 2.3483915 },
  { city: "Reims", latitude: 49.2577886, longitude: 4.031926 },
];

export class Weather implements WeatherAttributes {
  city: string;
  temperatureCelsius?: number;
  weatherCode?: number;

  /**
   * Constructeur de Weather à partir du nom d'une ville.
   *
   * @param {string} city - Le nom de la ville à instancier.
   */
  constructor(city: string) {
    this.city = city;
  }

  /**
   * Définit les informations météorologiques actuelles pour la ville associée à l'instance.
   * La méthode utilise les coordonnées géographiques de la ville pour récupérer les données météorologiques en temps réel depuis une API.
   *
   * @throws {Error} Si les coordonnées géographiques de la ville ne sont pas trouvées dans la liste prédéfinie.
   */
  async setCurrent() {
    const coordinates = COORDINATES_FOR_CITIES.find(
      (coordinates) =>
        coordinates.city.toLowerCase() === this.city.toLowerCase()
    );

    if (!coordinates) {
      throw new Error(`La ville "${this.city}" définie n'a pas été trouvée`);
    }

    const { latitude, longitude } = coordinates;

    const weatherResponse = await fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,weather_code`
    );

    const weather: WeatherData = await weatherResponse.json();

    this.city = coordinates.city;
    this.temperatureCelsius = weather.current.temperature_2m;
    this.weatherCode = weather.current.weather_code;
  }

  /**
   * Récupère la température pour une ville donnée.
   *
   * @returns {number} La température actuelle en degrés Celsius.
   */
  public getTemperatureForCity(): number {
    if (!this.temperatureCelsius) {
      throw new Error(
        `Les données pour "${this.city}" n'ont pas été définie. Il faut executer setCurrent().`
      );
    }

    return this.temperatureCelsius;
  }

  /**
   * Convertit une température de degrés Celsius en degrés Fahrenheit.
   *
   * @param {number} celsius - La température en degrés Celsius à convertir.
   * @returns {number} La température convertie en degrés Fahrenheit.
   */
  private static celsiusToFahrenheit(celsius: number): number {
    return (celsius * 9) / 5 + 32;
  }

  /**
   * Affiche les informations météorologiques formatées dans la console.
   *
   * @param {TemperatureUnit} [temperatureUnit="CELSIUS"] - L'unité de température à utiliser pour l'affichage.
   * @throws {Error} Si l'unité de température spécifiée n'est pas "CELSIUS" ou "FAHRENHEIT".
   */
  public print(temperatureUnit: TemperatureUnit = "CELSIUS"): void {
    if (!this.temperatureCelsius || !this.weatherCode) {
      throw new Error(
        `Les données pour "${this.city}" n'ont pas été définie. Il faut executer setCurrent().`
      );
    }

    const weatherDescription = CODES_METEO[this.weatherCode];
    let temperature: string;

    switch (temperatureUnit) {
      case "CELSIUS":
        temperature = this.temperatureCelsius + "°C";
        break;

      case "FAHRENHEIT":
        temperature =
          Weather.celsiusToFahrenheit(this.temperatureCelsius!) + "F";
        break;

      default:
        throw new Error(
          `L'unité de température "${temperatureUnit}" est inattendue`
        );
    }

    console.log(
      `${weatherDescription.text} à ${this.city} ${weatherDescription.icon}, la température est de ${temperature}.`
    );
  }
}
