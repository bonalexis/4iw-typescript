import { Weather } from "./Weather";
import express, { Handler } from "express";

const app = express();

const LILLE = "Lille";

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/weather", async (req, res) => {
  const weather = new Weather(LILLE);
  await weather.setCurrent();
  res.json(weather);
});

export default app;
