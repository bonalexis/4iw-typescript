import app from "./app";

const PORT = "3500";

app.listen(PORT, () => {
  console.log(`L'API écoute sur http://localhost:${PORT} !`);
});
